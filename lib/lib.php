<?php
/**
 *常用函数 
 */

//记录日志
function log_info($content,$file=""){	
	$rootdir	=	dirname(dirname(__FILE__));
	$date		=	date("Y-m-d");
	$now		=	date("Y-m-d H:i:s");
	$dateDir	= 	date("Y-m");
	if(!is_dir($rootdir."/log/$dateDir/")){ //2011-06-16 
		mkdir($rootdir."/log/$dateDir/");
	}
	$logFile	=	"";
	if ($file)	$logFile =	$rootdir."/log/$dateDir/".$file.$date.".txt";
	else	$logFile	=	$rootdir."/log/$dateDir/".$date.".txt";
	$fp	=	fopen($logFile,"a+");
	flock($fp, LOCK_EX);
	fwrite($fp,$now." ".$content."\n");
	flock($fp,LOCK_UN);
	fclose($fp);		
}
/**
 * 写文件 
 * @param  $fileName 文件名（不包含后缀）
 * @param  $content  文件内容
 * @param  $dir		   文件所在目录 
 * @param  $format	   文件格式 默认 txt
 * 
 * @return 返回生成文件路径
 */
function write_file($fileName="",$content,$dir="",$format=".txt"){	
	$rootdir	=	dirname(dirname(__FILE__));
	
	if(empty($dir)){
		$dateDir = date("Y-m");
		$dir=DS.'log'.DS.$dateDir.DS;	//目录为空时 为 /log/yyyy-m/目录
	}else {
		$endStr = substr($dir,-1,1);
		if($endStr !='/'&& $endStr !='\\'){	
			$dir .= DS;
		}
	}
	
	$fileDir	= $rootdir.$dir;
	if(!is_dir($fileDir)){ //2011-06-16 
		mkdir($fileDir);
	}
	$logFile = $fileDir.$fileName.$format;

	$fp	=	fopen($logFile,"a+");
	flock($fp, LOCK_EX);
	fwrite($fp,$content."\n");
	flock($fp,LOCK_UN);
	fclose($fp);
	
	return $logFile;
}

/**
 * 安全过滤字符
 * @param $str		安全过滤的字符串
 */
function safestr($str){
	if (get_magic_quotes_gpc()) {
		return $str;
	} else {
		if (is_array($str)) {
			foreach ($str as $k=>$val) {
				$str[$k] = preg_replace("/\\\\+/","\\", trim(str_replace(array("'",'"'), array("\'",'\"'), $val)));
			}
			return $str;
		} else {
			return preg_replace("/\\\\+/","\\", str_replace(array("'",'"'), array("\'",'\"'), $str));
		}
	}
}

/**
 * 获取客户端IP
 */

function getClientIP()
{
	if( isset($_SERVER) )
	{
		if( isset($_SERVER["HTTP_X_FORWARDED_FOR"]) )
		{
			$realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		} elseif ( isset($_SERVER["HTTP_CLIENT_IP"]) ) {
			$realip = $_SERVER["HTTP_CLIENT_IP"];
		} else {
			$realip = $_SERVER["REMOTE_ADDR"];
		}
	} else {
		if( getenv("HTTP_X_FORWARDED_FOR") )
		{
			$realip = getenv("HTTP_X_FORWARDED_FOR");
		} elseif ( getenv("HTTP_CLIENT_IP") ) {
			$realip = getenv("HTTP_CLIENT_IP");
		} else {
			$realip = getenv("REMOTE_ADDR");
		}
    }
    return addslashes($realip);
}

/**
 +----------------------------------------------------------
 * 直接跳转函数
 +----------------------------------------------------------
 * @access	public
 * @static
 +----------------------------------------------------------
 * @param string $url 跳转的地址
 +----------------------------------------------------------
 * @return void
 +----------------------------------------------------------
 */
function gotourl($url){
	if (headers_sent()){
		echo "<script language='JavaScript' type='text/javascript'>window.location.href='$url';</script>";
		die();
	}else{
		header("Location: ".$url);
		die();
	}
}

/**
 * 加密函数 
 * @param $data
 * @param $key
 */
function crypt_encode($data,$key)
{
	$size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_NOFB);

	mt_srand();
	$iv = mcrypt_create_iv($size, MCRYPT_RAND);
	$tmp = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_NOFB, $iv);
	return base64_encode($iv.$tmp);
}

/**
 * 解密函数 
 * @param $data
 * @param $key
 */
function crypt_decode($data,$key)
{
	$data = base64_decode($data);
	$size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_NOFB);

	$iv = substr($data, 0, $size);
	$data = substr($data, $size);

	return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_NOFB, $iv), "\0");
}

/**
 * 抓取页面或 获得返回参数
 * @param $url			请求URL
 * @param $data			参数
 * @param $is_post		1：post调用 	0:get调用
 */
function curl_request($url,$data=array(),$is_post=0){
	$exec_start = microtime(true);
	$curlPost 	= http_build_query($data);
	$curlPost 	= str_ireplace('amp;','',$curlPost);
	$ch 		= curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, $is_post);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 25);
	curl_setopt($ch, CURLOPT_TIMEOUT, 25);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
	$result 	= curl_exec($ch);
	
	curl_close($ch);
	
	$times = $exec_start - microtime(true);
	@log_info('curl: '. $url .' param: '. $curlPost .": ". $times .'s', 'remoteCall_');
	return $result;
}

?>
