<?php
//路由
if (PHP_SAPI === 'cli') {
	// Command line requires a bit of hacking
	if (isset($_SERVER['argv'][1]))
	{
		$current_uri = $_SERVER['argv'][1];

		// Remove GET string from segments
		if (($query = strpos($current_uri, '?')) !== FALSE)
		{
			list ($current_uri, $query) = explode('?', $current_uri, 2);

			// Parse the query string into $_GET
			parse_str($query, $_GET);
		}
	}
} elseif (current($_GET) === '' && substr($_SERVER['QUERY_STRING'], -1) !== '=') {
	// The URI is the array key, eg: ?this/is/the/uri
	$current_uri = key($_GET);

	// Remove the URI from $_GET
	unset($_GET[$current_uri]);

	// Remove the URI from $_SERVER['QUERY_STRING']
	$_SERVER['QUERY_STRING'] = ltrim(substr($_SERVER['QUERY_STRING'], strlen($current_uri)), '/&');
} elseif (isset($_SERVER['PATH_INFO']) && $_SERVER['PATH_INFO']) {
	//self::$current_uri = $_SERVER['PATH_INFO'];
	$current_uri = $_SERVER['REQUEST_URI'];
} elseif (isset($_SERVER['ORIG_PATH_INFO']) && $_SERVER['ORIG_PATH_INFO']) {
	$current_uri = $_SERVER['ORIG_PATH_INFO'];
} elseif (isset($_SERVER['PHP_SELF']) && $_SERVER['PHP_SELF']) {
	$current_uri = $_SERVER['PHP_SELF'];
}


$query_string = '';
if ( ! empty($_SERVER['QUERY_STRING'])) {
	$query_string = '?'.trim($_SERVER['QUERY_STRING'], '&/');
}

if ($current_uri === '/index.php') {
	$current_uri = 'index/main';
//	$default_route = TRUE;
}

$current_uri = preg_replace('#\.[\s./]*/#', '', $current_uri);
$current_uri = trim($current_uri, '/');
$segments = explode('/', $current_uri);

$class = isset($segments[0]) ? $segments[0] : 'index';
$action = isset($segments[1]) ? $segments[1] : 'main';
foreach( $segments as $key=>$val ) {
	$_GET[$key] = $val;
}
///----------------
$file = CONTROLLERS_PATH . preg_replace('/_|\./i',DS,$_GET[0]) . '.php';

if( !file_exists( $file ) )
{
	die('The server is busy, please try again later.');
}else {
	require_once $file;
}

$temp_var = end(preg_split('/_|\./i',$_GET[0]));

$c = new $temp_var;

if( !method_exists( $c, $_GET[1] ) )
{
	@log_info($class."->$action".' not exist!');//class的action方法不存在
	die('The server is busy, please try again later.');
}

$c->$_GET[1]();
$exeTime = microtime(true)-$execute_start;
@log_info($class.'->'.$action.': '.$exeTime.'s','ExecMethods_');

function __autoload ($class_name)
{
//	echo " class name: ".$class_name;
	if( is_file(ROOT_PATH."controllers".DS. $class_name . '.php') )	require_once ROOT_PATH."controllers".DS. $class_name . '.php';
	elseif(is_file(ROOT_PATH."models".DS. $class_name . '.php') ) require_once ROOT_PATH."models".DS. $class_name . '.php';
	else die('no load class '.$class_name);
}
?>
