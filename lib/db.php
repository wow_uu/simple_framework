<?php
/**
 * This is a PDO exntend class
 */

class DB extends PDO
{
	public static $charset  = "UTF8";
	public static $instance = array();
	public $debug           = TRUE;
	public $cache;
	//construct:connect database
	function __construct($key)
	{
		$key = $key ? $key : 'webgame_s';
		require CONFIG_PATH.'database.php';

		extract($config[$key]);

		$dsn = 'mysql:host=' . $host . ';dbname=' . $database;

		parent::__construct($dsn, $user, $pass);

		parent::exec('SET NAMES '.self::$charset.';');
		$this->cache = &$GLOBALS['memcach'];
	}

	/**
	 * Get a instance of MyPDO
	 *
	 * @param string $key
	 * @return object
	 */
    static function getInstance($key='webgame_s')
    {
		if (!self::$instance[$key]) {
			self::$instance[$key] = new DB($key);
		}
		return self::$instance[$key];
    }

	/**
	* Retrieve result of sql
	*
	* @param string $sql
	* @return array
	*/
	public function getResult($sql,$cache_key='',$exp=300, $type = PDO::FETCH_ASSOC)
	{
		if(empty($sql)) return false;
		
		if ( !empty( $cache_key ) && is_object($this->cache) )
		{
			if($rs = $this->cache->get($cache_key)){
					return $rs;
			}
		}
				
		$result = false;
		try {
			$pre = $this->prepare(trim($sql));
			if ( !is_object($pre) ) {
				log_info($sql.'\n SQL语句预处理失败:' . $this->errorCode() . "-" .implode(' ', $this->errorInfo()),'db_err_msg_');
				return false;
			}

			if ( !$pre->execute() ) {
				log_info($sql.'\n SQL语句执行失败:'. $this->errorCode() . "-"  . implode(' ', $this->errorInfo()),'db_err_msg_');
				return false;
			}

			$type = !empty($type) ? $type : PDO::FETCH_ASSOC;

			if ( $result = $pre->fetchAll($type)){
				if ( !empty( $cache_key ) && is_object($this->cache) ){
					 $this->cache->set($cache_key,$rs,$exp);
				}
				return $result;
			} else {
				return false;
			}

		} catch (PDOException $e) {
			if ($this->debug) {
				log_info(var_export($e,true),'db_err_msg_');
			}
		}

	}

	/**
	 * select data
	 *
	 * @param	string	table name
	 * @param	array	cols and values
	 * @param	array	order by
	 * @param	int		start
	 * @param	int		limit
	 * @param	bool	if true return rowcount
	 * @return	array
	 */
	public function select($table, $input = array(), $order = '', $start_num = NULL, $limit_num = NULL, $count = false)
	{
		if (!empty($input)) {
			$where_cols = array();
			foreach ($input as $key=>$value) {
				$where_cols[] = '`'. $key . '`' . ' = :' . $key;
			}
			$where = ' WHERE ' . implode(' AND ', $where_cols);
		} else {
			$where = '';
		}

		$orderby = !empty($order) && is_array($order) ? ' ORDER BY ' . implode(',', $order) : '';

		$limit   = isset($start_num) && isset($limit_num) ? ' LIMIT ' . intval($start_num) . ',' . intval($limit_num) : '';

		$sql = 'SELECT * FROM ' . $table . $where . $orderby . $limit;
		@log_info($sql,"sql_");
		try {
			$sth = $this->prepare($sql);
			if ( !$sth->execute($input) ) {
				$errInfo = $sth->errorInfo();
				throw new PDOException($errInfo[2], $errInfo[1]);
			}
            return $count ? $sth->rowCount() : $sth->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			if ($this->debug) {
				log_info(var_export($e,true),'db_err_msg_');
			}
			return false;
		}
	}

	/**
	 * insert data
	 *
	 * @param	string	table name
	 * @param	array	cols and values
	 * @return	last_insert_id or flase
	 */
	public function insert($table, $input, $method = 'INSERT')
	{
		$cols_name  = array();
		$cols_value = array();

		foreach ($input as $key=>$value) {
			$cols_name[]  = '`'.$key.'`';
			$cols_value[] = ':' . $key;
		}

		$sql = $method . ' ' . $table . ' (' . implode(',', $cols_name) . ') ' . 'VALUES (' . implode(',', $cols_value) . ') ';
		@log_info($sql,"sql_");
		try {
			$sth = $this->prepare($sql);
			if ( !$sth->execute($input) ) {
				$errInfo = $sth->errorInfo();
				throw new PDOException($errInfo[2], $errInfo[1]);
			}
            return $this->lastInsertId();
		} catch (PDOException $e) {
			if ($this->debug) {
				log_info(var_export($e,true),'db_err_msg_');
			}
			return false;
		}
	}

	/**
	 * delete data
	 *
	 * @param	string	table name
	 * @param	array	cols and values
	 * @return	boolean
	 */
	public function delete($table, $input = array())
	{
		if (!empty($input)) {
			$where_cols = array();
			foreach ($input as $key=>$value) {
				$where_cols[] = $key . ' = :' . $key;
			}
			$where = ' WHERE ' . implode(' AND ', $where_cols);
		} else {
			$where = '';
		}

		$sql = 'DELETE FROM ' . $table . $where;
        @log_info($sql,"sql_");
		try {
			$sth = $this->prepare($sql);
			if ( !$sth->execute($input) ) {
				$errInfo = $sth->errorInfo();
				throw new PDOException($errInfo[2], $errInfo[1]);
			}
            return true;
		} catch (PDOException $e) {
			if ($this->debug) {
				log_info(var_export($e,true),'db_err_msg_');
			}
			return false;
		}
	}

	/**
	 * update data
	 *
	 * @param	string	table name
	 * @param	array	update cols and values
	 * @param	array	where cols and values
	 * @return	boolean
	 */
	public function update($table, $input = array(), $condition = array())
	{
		//update cols
		$set_cols = array();
		foreach ($input as $key=>$value) {
			$set_cols[] = "`" . $key . "`" . ' = :' . $key;
		}
		$set = implode(',', $set_cols);

		//where cols
		if (!empty($condition)) {
			$where_cols = array();
			foreach ($condition as $key=>$value) {
				$where_cols[] = "`" . $key . "`" . "= '" . $value . "'";
			}
			$where = ' WHERE ' . implode(' AND ', $where_cols);
		} else {
			//不加条件直接返回false，防止误操作
			return false;
			$where = '';
		}

		$sql = 'UPDATE ' . $table . ' SET ' . $set . $where;
		@log_info($sql,"sql_");
		try {
			$sth = $this->prepare($sql);
			if ( !$sth->execute($input) ) {
				$errInfo = $sth->errorInfo();
				throw new PDOException($errInfo[2], $errInfo[1]);
			}
            return true;
		} catch (PDOException $e) {
			if ($this->debug) {
				log_info(var_export($e,true),'db_err_msg_');
			}
			return false;
		}
	}

	public function getOne($sql,$cache_key,$exp=300, $type = PDO::FETCH_ASSOC)
	{
		$result = $this->getResult($sql,$cache_key,$exp,$type);
		return $result[0];
	}
	
	public function selectOne($table, $input = array())
	{
		$result = $this->select($table, $input);
		return $result[0];
	}

	//Get The Num_Rows
	public function getCount($table, $input = array())
	{
		if (!empty($input)) {
			$where_cols = array();
			foreach ($input as $key=>$value) {
				$where_cols[] = $key . ' = :' . $key;
			}
			$where = ' WHERE ' . implode(' AND ', $where_cols);
		} else {
			$where = '';
		}

		$sql = 'SELECT COUNT(*) AS rows FROM ' . $table . $where;
		@log_info($sql,"sql_");
		try {
			$sth = $this->prepare($sql);
			if ( !$sth->execute($input) ) {
				$errInfo = $sth->errorInfo();
				throw new PDOException($errInfo[2], $errInfo[1]);
			}
            return intval( $sth->fetch(PDO::FETCH_OBJ)->rows );
		} catch (PDOException $e) {
			if ($this->debug) {
				log_info(var_export($e,true),'db_err_msg_');
			}
			return false;
		}
	}
}

?>