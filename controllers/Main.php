<?PHP

class Main 
{
	
	protected $_ci_ob_level ;
	protected $view;
	public function __construct()
	{
		$this->view = &$GLOBALS['tpl'];
	}
	
	/**
	 * 加载Model层，并返回model 实例！<br/>
	 * module 命名规则:文件夹名.类名
	 * @param $module	文件名、Model名要与文件名保持一至   
	 * @param $db		是否检查数据库连接（实例化数据库类）
	 */
	public function  loadModel($module,$db=true)
	{
//		echo $module,"<br/>";
		static $is_Module = array();
		if( array_key_exists($module, $is_Module) || (isset(base::get_instance()->$module) && is_object(base::get_instance()->$module)) )
		{
			if( !$this->$module )
			{
				$this->$module  = base::get_instance()->$module;
			}
			return base::get_instance()->$module;
		}
        
		$true_dir = str_replace('.',DS,$module);
//		echo $module," ",MODELS_PATH.$true_dir.'.php';
		if( is_file( MODELS_PATH.$true_dir.'.php' ) )
		{
			require_once( MODELS_PATH.$true_dir.'.php');
		} else {
			return false;
		}

//		//检测数据库连接
//		if( $db && !$this->db )
//		{
//			$this->db_link();
//		}
		$temp_mod = end(explode('.',$module));
		
		$mod = new $temp_mod();
		base::get_instance()->$module = $this->$module  = $mod;
		$is_Module[$module] = true;
		
		return $mod;
	}
	public function __destruct()
	{
		
	}
}

?>
