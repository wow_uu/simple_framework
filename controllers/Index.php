<?PHP

class Index extends Main
{
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * 不经model层直接查询数据库
	 */
	public function main(){
		$db = DB::getInstance('master');
		$rs = $db->getResult('select * from test');
		$this->view->assign('rs', $rs);
		$this->view->display('test.tpl');
	}
	
	/**
	 * 经过model层取数库，model文件在Models根目录下
	 */
	public function getData(){
		$mod = $this->loadModel('TestModel');
		$rs = $mod->getTestData();
		$this->view->assign('rs', $rs);
		$this->view->display('test.tpl');
	}
	
	/**
	 * 经过model层取数库，model文件在Models根目录下的test目录下
	 */
	public function getTestData(){
		$mod = $this->loadModel('test.TestModel');
		$rs = $mod->getTestData();
		$this->view->assign('rs', $rs);
		$this->view->display('test.tpl');
	}
	
	public function __destruct()
	{
	}
}
?>
