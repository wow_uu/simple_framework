<?php
@ini_set('session.auto_start',    0);
@ini_set('date.timezone','Asia/Shanghai');
@date_default_timezone_set('PRC');
@ini_set('display_errors',1);
define('LIB_PATH',ROOT_PATH.DS.'lib'.DS);
define('CONFIG_PATH',ROOT_PATH.DS.'config'.DS);

//路径
define("CONTROLLERS_PATH",ROOT_PATH.'controllers'.DS);
define("VIEW_PATH", ROOT_PATH.'view'.DS);
define("DATA_PATH", ROOT_PATH.'data'.DS);
define("MODELS_PATH", ROOT_PATH.'models'.DS);

require_once LIB_PATH.'validn.php';
require_once LIB_PATH.'lib.php';
require_once LIB_PATH.'db.php';
require_once CONFIG_PATH.'cache.php';
require_once CONFIG_PATH.'constants.php';
require_once CONFIG_PATH.'smarty.php';


//---session使用memcached替换，实现session共享---start
//cache
require (LIB_PATH .'cache'.DS. 'memcached.php');
$memcached = new memcached($cache_server);
require LIB_PATH .'session'.DS. 'session.php';
require LIB_PATH .'session'.DS. 'drivers'.DS.'session_memcache_driver.php';
$sess = new session_memcache_driver(DOMAIN);
$_SESSION = $sess->start();
//---session使用memcached替换，实现session共享---end

//加载Smarty
header('Cache-control: private');
header('Content-type: text/html; charset=utf-8');

/* Create Smarty Object */
require (LIB_PATH . 'smarty'.DS.'Smarty.class.php');
$tpl = new Smarty;
$tpl->left_delimiter	=	TPL_LEFT_DELIMITER;
$tpl->right_delimiter	=	TPL_RIGHT_DELIMITER;
$tpl->caching			=	TPL_CACHING;
$tpl->cache_lifetime	=	TPL_CACHE_LIFETIME;
$tpl->cache_dir			=	TPL_CACHE;
$tpl->template_dir		=	TPL_TEMPLATE;
$tpl->compile_dir		=	TPL_COMPILE;
$tpl->compile_check		=	true;
$tpl->register_function("insert_scripts", "insert_scripts");
$tpl->register_modifier("sslash","stripslashes");
?>
